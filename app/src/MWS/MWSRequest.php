<?php

namespace MWS;

/*
 MWS request class for performing signed http requests to amazon mws api
 */
class MWSRequest{

private $endpointUrl;
private $marketplaceId;

/**
 * [__construct description]
 * @param array $endpoint MWS endpoint array returned from MWSEndpoint::getEndpoint
 */
 public function __construct(array $endpoint){

     if(!array_key_exists("url", $endpoint) || !array_key_exists("marketplaceId", $endpoint)){
       throw new \Exception("Incorrect endpoint format!");
     }
     $this->endpointUrl = $endpoint["url"];
     $this->marketplaceId = $endpoint["marketplaceId"];
 }

/**
 * [computeSignature computing secret signature for request signing
 * according the http://docs.developer.amazonservices.com/en_US/dev_guide/DG_ClientLibraries.html#DG_OwnClientLibrary__Signatures]
 * @param  string $queryString query string for hash
 * @param  string $url full request url, with method and version
 * @return string $signature  base-64 decoded hash
 */
 private function computeSignature($dataParams, $url){

     $params = $dataParams;
     uksort($params, 'strcmp'); //sorting the data array
     $queryString = array();

     foreach ($params as $key => $value) {
       if(!empty($value) && !empty($key)){
         $queryString[] = $key . "=" . rawurlencode($value);
       }
     }

     $queryData = array(
       "POST",
       $this->endpointUrl,
       $url,
       implode("&",$queryString), //query string formed according to amazon signature scheme
     );

     $query = implode("\n",$queryData);

     $hash =  hash_hmac("sha256", $query, \MWS\MWSConfig::$SECRET_KEY);
     $hash = base64_encode($hash);
     return $hash;
 }

/**
 * getUserAgent generation of user-agent, reccomended by Amazon
 * According to http://docs.developer.amazonservices.com/en_US/dev_guide/DG_ClientLibraries.html
 * @return string $userAgent User-agent string generated from amazon scheme
 */
private function getUserAgent(){
  $userAgent = "x-amazon-user-agent:";
  $userAgent.= \MWS\MWSConfig::$APPLICATION_NAME;
  $userAgent.= "/".\MWS\MWSConfig::$APPLICATION_VERSION;
  $userAgent.= " (Language=PHP)";

  return $userAgent;
}



/**
 * send sends request to MWS endpoint and returns an XML result as SimpleXMLElement
 * @param  string $method  Amazon MWS Action
 * @param  string $version Api version for following action
 * @param  string $data   Form-data for request (w/o common required fields)
 * @throws Exception      HTTP or MWS error occured during request
 * @return SimpleXMLElement         XML response document
 */
 public function send($method, $version = "", $data){
    $requestUrl  = $this->endpointUrl . "/" . $method . "/" . $version;


    //adding neccesary request params to request
    //according to http://docs.developer.amazonservices.com/en_US/dev_guide/DG_RequiredRequestParameters.html

    $data["AWSAccessKeyId"] = \MWS\MWSConfig::$ACCESS_KEY_ID;
    $data["MWSAuthToken"] = \MWS\MWSConfig::$AUTH_TOKEN;
    $data["Action"] = $method;
    $data["SignatureVersion"] = "2"; //according the amazon request signing docs
    $data["SignatureMethod"] = "hmacSHA256"; // recommended value
    $data["Timestamp"] = gmdate("Y-m-d\TH:i:s.\\0\\0\\0\\Z", time());
    $data["Version"] = $version;
    $data["SellerId"] = \MWS\MWSConfig::$SELLER_ID;

    $signature = $this->computeSignature($data, $requestUrl);

    $data["Signature"] = $signature;

    $ch = curl_init();
        curl_setopt( $ch, CURLOPT_URL, $requestUrl );
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );
        curl_setopt( $ch, CURLOPT_POST, 1 );
        curl_setopt( $ch, CURLOPT_POSTFIELDS, http_build_query($data) );
        curl_setopt( $ch, CURLOPT_USERAGENT, $this->getUserAgent());
        $xmlResponse = curl_exec($ch);
        $statusResponse = curl_getinfo($ch);
        curl_close($ch);


    if($statusResponse["http_code"]===200 && !curl_errno($ch)){

       $responseObject = simplexml_load_string($xmlResponse);
       if(isset($responseObject->{'Error'})){ //error occured, forming exception with full info
          $type = $responseObject->{'Error'}->{'Type'};
          $code = $responseObject->{'Error'}->{'Code'};
          $message = $responseObject->{'Error'}->{'Message'};
          $detail = $responseObject->{'Error'}->{'Detail'};
          throw new \MWSResponseException($message, $type, $code, $detail);
       }else{
         return $responseObject;
       }

       return $xmlResponse;
    }else{
      throw new \Exception("HTTP Error ({$statusResponse['http_code']})");
    }



 }




}





?>
