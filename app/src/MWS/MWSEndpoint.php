<?php

namespace MWS;

class MWSEndpoint{

  /*

   MWS API Endpoints array, according to http://docs.developer.amazonservices.com/en_US/dev_guide/DG_Endpoints.html
   TODO : Add all endpoints to array

  */
  public static $endpoints = array(
      "US" => array(
        "url" => "https://mws.amazonservices.com",
        "marketplaceId" => "ATVPDKIKX0DER"
      ),

      "JP" => array(
        "url" => "https://mws.amazonservices.jp",
        "marketplaceId" => "A1VC38T7YXB528"
      )
  );

/**
 * getEndpoint get MWS api endpoint by country code
 * @param  string $countryCode country code in 2-symbol format
 * @return array  $endpoint array containing url and marketplaceId
 * @throws Exception if endpoint does not exists
 */
  public static function getEndpoint($countryCode){
    if (array_key_exists($countryCode, self::$endpoints)){
      return self::$endpoints[$countryCode];
    }else{
      throw new \Exception("Endpoint for $countryCode does not exist!");
    }
}

}



?>
