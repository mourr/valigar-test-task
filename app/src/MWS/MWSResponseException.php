<?php

namespace MWS;


class MWSResponseException extends Exception{

  /**
   * [__construct MWSResponseException constructor with custom params
   * @param string  $message  exception message
   * @param string $type     amazon error type
   * @param string  $MWSCode  amazon error name
   * @param string  $detail   amazon error details
   * @param integer $code     base Exception code (default = 0)
   * @param Exception  $previous base Exception previous exception in chain (default = null)
   */
  public function __construct($message, $type, $MWSCode, $detail, $code = 0, Exception $previous = null) {

      $message.= "\r\nType : ".$type;
      $message.= "\r\nCode : ".$MWSCode;
      $message.= "\r\nDetail : ".$detail;

       parent::__construct($message, $code, $previous);
   }

}





?>
