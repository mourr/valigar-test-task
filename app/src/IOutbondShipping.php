<?php

interface IOutbondShipping {

    /**
     * Need to realize logic that will send command to Amazon FBA to ship order
     * and will return tracking number as string for this order.
     * if operation cannot be performed please throw Exception with error message
     * @param \Order $oOrder
     * @param \Buyer $oBuyer
     * @return array of strings Tracking number and Carrier ID must be returned
     * @throws Exception
     */
    public function ship(\Order $oOrder,\Data\Raw\Buyer $oBuyer);
}
