<?php


class OutboundShipping implements \IOutbondShipping{


  /**
   * Checking if outbound order exists, and creating it if neccesary
   * If possible (status is SHIPED) returns track info
   * @param  Order        $oOrder Oreder data object
   * @param  DataRawBuyer $oBuyer Buyer data object
   * @throws Exception   HTTP or MWS error occured during request
   * @return Array of items data (status and optionaly TrackNumber and carrierID)
   */
   public function ship(\Order $oOrder, \Data\Raw\Buyer $oBuyer){
      $data = array();

      $endpoint = \MWS\MWSEndpoint::getEndpoint($oBuyer->get_country_code());

      /**
       * @see function getOrderInfo
       * NOTE getOrderInfo now uses ResponseTestData
       */
      $orderInfo = $this->getOrderInfo($endpoint,$oOrder->data["order_unique"]);
      if(sizeof($orderInfo) > 0){
        return $orderInfo;
      }else{

        // if not exists - create order and return pending status
        // NOTE Amazon MWS FBA Api assigns track and carrierID only when SHIPPED status set
        // NOTE createOutboundOrder now uses ResponseTestData
        $order = $this->createOutboundOrder($oOrder, $oBuyer, $endpoint);
        return array(
          "itemId" => $oOrder->data["order_unique"],
          "status" => "PENDING"
        );
      }


   }



   /**
    * getOrderInfo reusable code interface for GetFulfillmentOrder
    * @param  array $endpoint endpoint returned by MWSEndpoint::getEndpoint
    * @param  string $orderId  arbitrary order id, obtained when order is created
    * @throws Exception      HTTP or MWS error occured during request
    * @return Array of items data (status and optionaly TrackNumber and carrierID)
    */
   private function getOrderInfo($endpoint, $orderId){
     $request = new \MWS\MWSRequest($endpoint);

     $data = array();
     $data["SellerFulfillmentOrderId"] = "extern_id_".$orderId; /* extern_id_* according to
     http://docs.developer.amazonservices.com/en_US/fba_outbound/FBAOutbound_GetFulfillmentOrder.html
     */

      /*
      Sending request
      $orderObject = $request->send("GetFulfillmentOrder","2010-10-01",$data);
     */

     /* Load test data */
     $orderObject  = simplexml_load_string(\ResponseTestData::getGetFullfillmentOrderResponse());

     $itemsInfo = array();

     if(isset($orderObject->{"GetFulfillmentOrderResult"}->{"FulfillmentShipment"})){
       foreach ($orderObject->{"GetFulfillmentOrderResult"}->{"FulfillmentShipment"}->{"member"} as $item) {

         $trackingInfo = array();
         $itemStatus = $item->{"FulfillmentShipmentStatus"};
         $itemId = $item->{"FulfillmentShipmentItem"}->{"member"}[0]->{"SellerFulfillmentOrderItemId"};

         $trackingInfo["itemId"] = strval(trim($itemId));
         $trackingInfo["status"] = strval($itemStatus);



         if($itemStatus == "CANCELLED_BY_FULFILLER" || $itemStatus == "CANCELLED_BY_SELLER"){
           throw new Exception("Order $orderId was canceled with status $packageStatus");
         }


         if($itemStatus == "SHIPPED"){

           foreach($item->{"FulfillmentShipmentPackage"} as $package) {
                foreach($item->{"FulfillmentShipmentPackage"}->{"member"} as $packageMember) {
                      if(isset($packageMember->{"TrackingNumber"})) {

                          $trackingInfo["trackingNumber"] = strval($packageMember->{"TrackingNumber"});
                          $trackingInfo["carrierCode"] = strval($packageMember->{"CarrierCode"});
                      }

                  }
              }

         }

     $itemsInfo[] = $trackingInfo;
       }
     }

     return $itemsInfo;

   }

   /**
    * createOutboundOrder create and send to FBA an outbound order with items from $order and data from $buyer
    * @param  Order        $order    Order data object
    * @param  DataRawBuyer $buyer    Buyer data object
    * @param  array        $endpoint  MWS API endpoint array, returned by MWSEndpoint::getEndpoint()
    * @throws Exception      HTTP or MWS error occured during request
    * @return SimpleXMLElement        MWS API response
    */
   private function createOutboundOrder(\Order $order, \Data\Raw\Buyer $buyer, array $endpoint){

     $request = new \MWS\MWSRequest($endpoint);

     /**
      * $data creating data array for request with neccesary params
      */
      $data = array(
        "MarketplaceId" => $endpoint["marketplaceId"],
        "SellerFulfillmentOrderId" =>"extern_id_".$order->data['order_unique'],
        "FulfillmentAction" => "Ship",
        "DisplayableOrderId" => $order->data['order_id'],
        "DisplayableOrderDateTime" => $order->data['order_date'],
        "ShippingSpeedCategory" => "Standard",
        "FulfillmentPolicy" =>"FillAll",
        "NotificationEmailList.member.1" => $buyer['email'],
      );

      $data["DestinationAddress.Name"] = $order->data["buyer_name"];
      $data["DestinationAddress.Line1"] = $order->data["shipping_street"];
      $data["DestinationAddress.CountryCode"] = $buyer->get_country_code();
      $data["DestinationAddress.City"] = $order->data["shipping_city"];
      $data["DestinationAddress.StateOrProvinceCode"] = $order->data["shipping_state"];
      $data["DestinationAddress.PostalCode"] = $order->data["shipping_zip"];
      $data["DestinationAddress.PhoneNumber"] = $buyer["phone"];



      /**
       * Iteratively forming items list for request
       */
      $products = $order->data['products'];

      for($i=0;$i<sizeof($products);$i++){

        $memeber = "Items.member".$i;
        $product = $products[$i];

        $data[$member."DisplayableComment"] = $product["comment"];
        $data[$member."Quantity"] = $product["amount"];
        $data[$member."SellerFulfillmentOrderItemId"] = $product["product_id"];
        $data[$memeber."SellerSKU"] = $product["sku"];

        if(array_key_exists("gift_message", $product)){
          $data[$member."GiftMessage"] = $product["gift_message"];
        }


      }

      /**
       * Processing request by MWSRequest
       * @var $method  action name for AWS API
       * @var $version API version for action
       * @var $data request data array
       */
      //$orderResponse = $request->send("CreateFulfillmentOrder","2010-10-01",$data);

      $orderResponse = simplexml_load_string(ResponseTestData::getCreateFulfillmentOrderResponse());

      return $orderResponse;


   }




}



?>
